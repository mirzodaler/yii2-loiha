<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    
    'language' => 'tg',

    'name'=> 'Технологияи сохтани интернет замимаҳо',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'main.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],

        
        'view' => [
            'class' => 'yii\web\View',
            /*
            'theme' => [
              'class' => 'yii\base\Theme',
              'pathMap' => ['@app/views' => 'themes/material-default'],
              'baseUrl'   => 'themes/material-default',
              //'pathMap' => ['@app/views' => 'themes/material-simple'],
              //'baseUrl'   => 'themes/material-simple',
              //'pathMap' => ['@app/views' => 'themes/metro'],
              //'baseUrl'   => 'themes/metro'
            ]
            */
        ],
        
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'yii2',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                ]
        ],
 /*       
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
 */       

        'cache' => [
            'class' => 'yii\caching\DbCache',
            'db' => 'db',
            'cacheTable' => 'my_cache',
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
           // 'suffix'=>'.html',
            'rules' => [
                '<alias:\w+>' => 'site/<alias>',
               ['class' => 'yii\rest\UrlRule', 'controller' => 'product', 'pluralize'=>true, 'extraPatterns' => ['GET search' => 'search']],
            ],
        ],
        
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', 'localhost', '::1'],
    ];
}

return $config;
