<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class DaoController extends Controller
{
    /**
    * Намунаи истифодабарии DAO
    */

    public function actionIndex(){
        echo "123";
    }
    public function actionNamuna()
    {
        //Гирифтани рӯйхати ҳамаи китобҳо
        //echo 'Гирифтани рӯйхати ҳамаи китобҳо:  $kitobho = Yii::$app->db->createCommand("SELECT * FROM kitobho")->queryAll();';
        $kitobho = Yii::$app->db->createCommand("SELECT * FROM kitobho")->queryAll();
        //var_dump($kitobho);
        echo "<br/>";
        echo sizeof($kitobho);
        
        /*
        foreach($kitobho as $kitob){
            echo $kitob["nomiKitob"];
            echo "<br/>";
        }
        
        //Гирифтани 1 китоб бо истифодаи bindValue, ки номаш PHP аст.
        //echo "Гирифтани 1 китоб бо истифодаи bindValue, ки номаш PHP аст.";
        $kitob = Yii::$app->db->createCommand("SELECT * FROM kitobho WHERE nomiKitob=:nom")->bindValue(":nom", "PHP")->queryOne();
        if($kitob){
            echo $kitob["sol"];
        }else{
            echo "Chunin kitob nest";
        }
        //var_dump($kitob);
        */

    }
}
