<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\MyKitob;
use app\models\Kitob;
use app\models\Student;

class SiteController extends Controller
{
    public function actionKitob(){
        
        $kitob = new MyKitob();
        $kitob->nomiKitob = "Yii2";
        $kitob->sol=2017;
        $kitob->muallifho="Qiang";
        
        if($kitob->save()){
            echo "Kitobi nav sabt shud: ".$kitob->idKitob;
        }else{
            echo "Khatogi";
        }
    }

    public function actionDeletestudent(){
        $student=Student::findOne(1);
        $student->delete();
    }

    public function actionUpdatestudent(){
        $student=Student::findOne(2);
        $student->nom ="Nomi nav";
        $student->save();
    }

    public function actionStudent(){
        $studentho = Student::find()->all();
        foreach($studentho as $student){
            echo $student->nom;
            echo " -> ";
            echo $student->shahr->nomiShahr;
            echo "<br/>";

        } 
    }

    public function actionFindkitob(){
       $kitobho = MyKitob::find()->all();
       foreach($kitobho as $kitob){
           echo $kitob->nomiKitob;
           echo "<br/>";
       } 

       $kitobhoiYii = MyKitob::findAll([
        'nomiKitob' => "Yii2",
        ]);
        var_dump($kitobhoiYii);
    }

    public function actionVorid()
    {
        $model = new EntryForm();
            
        if ($model->load(Yii::$app->request->post()) &&
        $model->validate()) {
            $natija = $model->sabt();
            $model=new EntryForm();
            return $this->render('vorid-forma', ['model' => $model, "natija"=>$natija]);
        } else {
            return $this->render('vorid-forma', ['model' => $model]);
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    public function actionTest()
    {
        return $this->render('test');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
