-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2017 at 12:34 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2-loiha`
--
CREATE DATABASE IF NOT EXISTS `yii2-loiha` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `yii2-loiha`;

-- --------------------------------------------------------

--
-- Table structure for table `kitobho`
--

CREATE TABLE `kitobho` (
  `idKitob` int(11) NOT NULL,
  `nomiKitob` varchar(100) NOT NULL,
  `sol` int(11) NOT NULL,
  `muallifho` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitobho`
--

INSERT INTO `kitobho` (`idKitob`, `nomiKitob`, `sol`, `muallifho`) VALUES
(4, 'PHP', 2016, 'Test2'),
(3, 'MySQL', 2015, 'Test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kitobho`
--
ALTER TABLE `kitobho`
  ADD PRIMARY KEY (`idKitob`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kitobho`
--
ALTER TABLE `kitobho`
  MODIFY `idKitob` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
