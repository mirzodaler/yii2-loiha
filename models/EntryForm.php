<?php

namespace app\models;

use yii\base\Model;
use Yii;

class EntryForm extends Model
{
    public $name;
    public $nasab;
    public $pochta;

    public function rules()
    {
        return [
        [['name', 'pochta', 'nasab'],    'required', 'message' => 'Марзҳо бояд пур карда шаванд.'],
        ['pochta',              'email'],
        ];
    }

    public function attributeLabels() {
        return [
          'nasab' => 'Насабро ворид кунед',
        ];
    }

    public function sabt(){
        return Yii::$app->db->createCommand()->insert('users', [
            'nom' => $this->name,
            'nasab' => $this->nasab,
            'pochta' => $this->pochta,
            ])->execute();
    } 
}
