<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kitobho".
 *
 * @property integer $idKitob
 * @property string $nomiKitob
 * @property integer $sol
 * @property string $muallifho
 */
class Kitob extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitobho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomiKitob', 'sol', 'muallifho'], 'required'],
            [['sol'], 'integer'],
            [['nomiKitob', 'muallifho'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idKitob' => 'Id Kitob',
            'nomiKitob' => 'Nomi Kitob',
            'sol' => 'Sol',
            'muallifho' => 'Muallifho',
        ];
    }
}
