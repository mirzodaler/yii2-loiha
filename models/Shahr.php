<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shahrho".
 *
 * @property integer $idShahr
 * @property string $nomiShahr
 */
class Shahr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shahrho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomiShahr'], 'required'],
            [['nomiShahr'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idShahr' => 'Id Shahr',
            'nomiShahr' => 'Nomi Shahr',
        ];
    }
}
