<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "students".
 *
 * @property integer $idStudent
 * @property string $nom
 * @property integer $shahrId
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }

    public function getShahr()
    {
        return $this->hasOne(Shahr::className(), ['idShahr' => 'shahrId'])  ;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom', 'shahrId'], 'required'],
            [['shahrId'], 'integer'],
            [['nom'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idStudent' => 'Id Student',
            'nom' => 'Nom',
            'shahrId' => 'Shahr ID',
        ];
    }
}
