<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kitob */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kitob-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomiKitob')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sol')->textInput() ?>

    <?= $form->field($model, 'muallifho')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
