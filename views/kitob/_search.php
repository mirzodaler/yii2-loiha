<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KitobSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kitob-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idKitob') ?>

    <?= $form->field($model, 'nomiKitob') ?>

    <?= $form->field($model, 'sol') ?>

    <?= $form->field($model, 'muallifho') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
