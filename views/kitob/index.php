<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KitobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kitobs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitob-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kitob', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idKitob',
            'nomiKitob',
            'sol',
            'muallifho',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
