<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kitob */

$this->title = 'Update Kitob: ' . $model->idKitob;
$this->params['breadcrumbs'][] = ['label' => 'Kitobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idKitob, 'url' => ['view', 'id' => $model->idKitob]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kitob-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
