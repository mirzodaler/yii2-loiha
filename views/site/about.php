<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\bootstrap\Progress;

use kartik\datetime\DateTimePicker;


$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
<?php
    echo Button::widget([
        'label' => 'Action 23423423',
        'options' => ['class' => 'btn-lg'],
    ]);

    echo Progress::widget([
        'percent' => 99,
        'label' => 'Test 123....',
        'barOptions' => ['class' => 'progress-bar-danger']
    ]);

    echo '<br/><label>Start Date/Time</label>';
    echo DateTimePicker::widget([
        'name' => 'datetime_10',
        'options' => ['placeholder' => 'Select operating time ...'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'd-M-Y g:i A',
            'startDate' => '01-Mar-2014 12:00 AM',
            'todayHighlight' => true
        ]
    ]);

?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>

    <br/> <br/>
    <?php
    
    if ($this->beginCache("tasodufi", ['duration' => 5])) {
        echo "Qimati tasodufii man: ".rand();
        $this->endCache();
    }

    ?>

</div>
