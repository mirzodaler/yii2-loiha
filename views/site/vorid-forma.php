<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php
if (isset($natija)) {
    echo "Natijai shumo ".$natija;
}
?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name') -> label('Номатонро ворид кунед') ?>
<?= $form->field($model, 'nasab') ?>
<?= $form->field($model, 'pochta') ?>

<div class="form-group">
    <?= Html::submitButton('Фиристодан', ['class' => 'btn btn-primary']);?>
</div>

<?php ActiveForm::end(); ?>
